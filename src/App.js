import React from 'react';

import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import {TimeProvider} from "./components/TimeProvider";
import { HeaderMenu } from './modules/HeaderMenu';
import { ToDo } from './modules/ToDo';
import { AppLayout } from './components/AppLayout';

class App extends React.Component {

    render() {
        return (
            <AppLayout>
                <TimeProvider>
                    <HeaderMenu />
                    <ToDo />
                </TimeProvider>
            </AppLayout>
        );
    };
}

export default App;
