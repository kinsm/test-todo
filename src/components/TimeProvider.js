import React from 'react';

import {TimeContext} from "./TimeContext";

class TimeProvider extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            time: null,
        };
    }

    componentDidMount() {
        this.timeTimer = setInterval(this.updateTimeState, 1000);

        this.setState(
            {
                time: new Date().toLocaleString(),
            });
    }

    componentWillUnmount() {
        clearInterval(this.timeTimer);
    }

    updateTimeState = ()=> {
        this.setState({
            time: new Date().toLocaleString()
        })
    };

    render() {
        return (
            <TimeContext.Provider value={this.state.time}>
                {this.props.children}
            </TimeContext.Provider>
        );
    };
}

export { TimeProvider };
