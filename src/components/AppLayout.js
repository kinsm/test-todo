import React from 'react';

function AppLayout({ children }) {
    return (
        <div className="App">
            {children}
        </div>
    );
}

export {
    AppLayout,
}