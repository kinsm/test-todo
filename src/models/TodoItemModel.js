
export default class TodoItemModel {
    constructor() {
        const items = localStorage.getItem('todoItems');

        const sampleItems = [
            {
            title:"create react app1"
            },
            {
                title:"create react app 2"
            }
            ];

        if (!items) {
            localStorage.setItem('todoItems', JSON.stringify(sampleItems))
        }

        this.setItems();
    }

    setItems() {
        this.items = JSON.parse(localStorage.getItem('todoItems'));
    }

    getTodoItems() {
        return this.items;
    };

    addTodoItem(item) {
        this.items = [...this.items, {title: item}];
        localStorage.setItem('todoItems', JSON.stringify(this.items));
        this.setItems();

        return true;
    };

    removeItem (id) {
        console.log('model', id, this.items);

        this.items.splice(id, 1);

        localStorage.setItem('todoItems', JSON.stringify(this.items));
        this.setItems();
        return true;
    }
}
