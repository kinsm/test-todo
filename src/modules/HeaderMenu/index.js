import React from 'react';
import {TimeContext} from "../../components/TimeContext";
import {Nav, Navbar} from "react-bootstrap";
import logo from "../../logo.svg";

function HeaderMenu() {
    return (
        <>
            <Navbar bg="dark" variant="dark">
                <Navbar.Brand href="#home">
                    <img
                        alt=""
                        src={logo}
                        width="30"
                        height="30"
                        className="d-inline-block align-top"
                    />{' '}

                </Navbar.Brand>
                <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                <Navbar.Collapse id="responsive-navbar-nav">
                    <Nav className={""}>
                        <Nav.Link href="#deets">TODO APP</Nav.Link>
                        <Nav.Link href="#">
                            <TimeContext.Consumer>
                                {(time)=> time}
                            </TimeContext.Consumer>
                        </Nav.Link>
                    </Nav>
                </Navbar.Collapse>
            </Navbar>
        </>
    );
}

export {
    HeaderMenu,
}