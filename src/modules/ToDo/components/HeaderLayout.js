import React from 'react';

function HeaderLayout({ children }) {
    return (
        <div className={"mt-3"}>
            {children}
        </div>
    );
}

export { HeaderLayout }