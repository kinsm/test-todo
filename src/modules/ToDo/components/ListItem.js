import React from 'react';
import {Button} from "react-bootstrap";

function ListItem({
    id,
    item,
    onClick,
}) {
    return (
        <tr key={id}>
            <td>{id}</td>
            <td>{item.title}</td>
            <td>
                <Button variant={"danger"} onClick={() => onClick(id)}>Удалить</Button>
            </td>
        </tr>
    );
}

export { ListItem };
