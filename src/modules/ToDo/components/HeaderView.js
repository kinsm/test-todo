import React from 'react';
import {AlertError} from "../components/AlertError";
import {Button, InputGroup, FormControl} from "react-bootstrap";

function HeaderView({
    onChange,
    value,
    onButtonClick,
    error,
}) {
    return (
        <div>
            <InputGroup className="mb-3">
                <FormControl
                    placeholder="Введите задание"
                    aria-label="Recipient's username"
                    aria-describedby="basic-addon2"
                    onChange={onChange}
                    value={value}
                />
                <InputGroup.Append>
                    <Button onClick={onButtonClick} variant={"primary"}>Добавить</Button>
                </InputGroup.Append>
            </InputGroup>
            <AlertError error={error}/>
        </div>
    );
}

export { HeaderView };