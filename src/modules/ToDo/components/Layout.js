import {Container} from "react-bootstrap";
import React from "react";

function Layout({ children }) {
    return (
        <Container className={"mt-5 text-left"}>
            {children}
        </Container>
    )
}

export { Layout }