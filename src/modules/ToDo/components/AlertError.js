import {Alert} from "react-bootstrap";
import React from "react";

function AlertError(props) {
    if (props.error) {
        return <Alert  variant={"danger"}>
            Ошибка: {props.error}
        </Alert>;
    } else {
        return null;
    }
}

export {
    AlertError,
};