import {Table} from 'react-bootstrap';
import React from 'react';

class ListView extends React.Component {
    render() {
        return (
            <Table striped bordered hover>
                <thead>
                <tr>
                    <th>#</th>
                    <th>Задача</th>
                    <th>Действие</th>
                </tr>
                </thead>
                <tbody>
                    {this.props.children}
                </tbody>
            </Table>
        );
    }

}

export { ListView };


