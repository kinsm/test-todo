import {Header, List} from "./containers";
import React from "react";
import {Layout} from './components/Layout';
import TodoItemModel from "../../models/TodoItemModel";

class ToDo extends React.Component {
    constructor(props) {
        super(props);
        this.itemsModel = new TodoItemModel();
        this.state = {
            todoItems: [],
            addError: null,
        };
    }

    componentDidMount() {
        this.setState(
            {
                todoItems: this.itemsModel.getTodoItems(),
                addError: null,
            });
    }

    addItemToStore = (title) => {

        if (title.length > 0) {

            if (this.itemsModel.addTodoItem(title)) {
                this.setState({
                    todoItems: [...this.itemsModel.getTodoItems()],
                    addError: null,
                });
                return true
            }
        } else {
            this.setState({
                addError: "Введите название"
            });
        }

        return false;

    };

    removeItem = (id) => {
        if (this.itemsModel.removeItem(id)) {
            this.setState({
                todoItems: [...this.itemsModel.getTodoItems()]
            })
        }
    };

    render() {
        return (
            <Layout>
                <Header
                    addError={this.state.addError}
                    addItemToStoreHandler={this.addItemToStore}
                />
                <List
                    handleDeleteItem={this.removeItem}
                    todoItems={this.state.todoItems}
                />
            </Layout>)
    }
}

export { ToDo }
