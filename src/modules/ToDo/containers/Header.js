import React from "react";
import { HeaderView, HeaderLayout, Time} from '../components';
import {TimeContext} from "../../../components/TimeContext";

class Header extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            inputItem: ""
        };
    }

    componentDidMount() {
        this.setState({
            inputItem: ""
        })
    }

    handleAddClick = () => {
        if (this.props.addItemToStoreHandler(this.state.inputItem)) {
            this.setState({
                inputItem: ""
            });
        } else {
            console.log("Input error")
        }
        console.log('click')
    };

    handleChangeInputItem = (event) => {
        this.setState({
            inputItem: event.target.value
        });
        console.log('input item', this.state.inputItem)
    };

    render() {
        return (
            <HeaderLayout>
                <HeaderView
                    onChange={this.handleChangeInputItem}
                    value={this.state.inputItem}
                    onButtonClick={this.handleAddClick}
                    error={this.props.addError}
                />
                <TimeContext.Consumer>
                    {(time)=>
                        <Time time={time} />

                    }
                </TimeContext.Consumer>
            </HeaderLayout>
        );
    }
}

export { Header }

