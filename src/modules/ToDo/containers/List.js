import React from 'react';
import { ListView, ListItem } from '../components';

class List extends React.Component {
    handleDeleteItem = (id) => {
        this.props.handleDeleteItem(id);
        console.log(id)
    };

    render() {
        return (
            <ListView>
                {
                    this.props.todoItems.map(
                        (item, id) =>
                            <ListItem
                                key={id}
                                id={id}
                                item={item}
                                onClick={this.handleDeleteItem}
                            />
                    )
                }
            </ListView>
        );
    }

}

export { List };

